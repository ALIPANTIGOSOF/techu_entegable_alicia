package com.entregable.modelo;

public class ModeloProducto {
    private int id;
    private String marca;
    private String descripcion;
    private double precio;
    private String [] usuario;

    public ModeloProducto(int id, String marca, String descripcion, double precio, String[] usuario) {
        this.id = id;
        this.marca = marca;
        this.descripcion = descripcion;
        this.precio = precio;
        this.usuario = usuario;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String[] getUsuario() {
        return usuario;
    }

    public void setUsuario(String[] usuario) {
        this.usuario = usuario;
    }
}
