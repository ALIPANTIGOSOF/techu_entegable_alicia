package com.entregable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EntegablesApplication {

	public static void main(String[] args) {
		SpringApplication.run(EntegablesApplication.class, args);
	}

}
